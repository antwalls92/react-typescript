import { Component, OnInit } from '@angular/core';
import { Product } from '../product/models/product';
import { ShoppingCart } from './models/shopping-cart';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {


  shoppingCart: ShoppingCart;
  shoppingCart$: Observable<ShoppingCart>;

  private store: Store<ShoppingCart>

  constructor(store: Store<ShoppingCart>) {
    this.shoppingCart = new ShoppingCart();
    this.store = store;

    this.store
      .subscribe(val => this.updateShoppingCart(val));
  }

  updateShoppingCart(state) {
    this.shoppingCart = state.shoppingCart;
  }

  ngOnInit() {
    this.shoppingCart = new ShoppingCart();
  }

  addProduct(product: Product): void {
    this.shoppingCart.addProduct(product);
  }

  removeProduct(idProduct: number): void {
    this.shoppingCart.removeProduct(idProduct);
  }

}
