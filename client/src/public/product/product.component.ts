import { Component, OnInit } from '@angular/core';
import { ProductService } from './services/product.service';
import { Product } from './models/product';
import { Store } from '@ngrx/store';

import { ShoppingCart } from '../shopping-cart/models/shopping-cart';
import { AddProduct } from 'src/app/core/store/state/shopping-cart/shopping-cart';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'], 
})
export class ProductComponent implements OnInit {

  products: Product[];
  private store: Store<ShoppingCart>


  constructor(private productService: ProductService, store: Store<ShoppingCart>) {
    this.store = store;
  }

  ngOnInit() {
    this.getProducts();
  }

  getProducts(): void {
    this.productService
      .getProducts()
      .subscribe(
        products => (this.products = products)
      )
  }

  addProductToShoppingCart(product: Product): void {
    this.store.dispatch(new AddProduct(product))
  }


}
