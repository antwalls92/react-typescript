import { Product } from './product'

export const PRODUCTS: Product[] = [
  { id: 1, name: "Ibanez RG 870", price: 670.35, photo: "http://www.guitare-live.com/cours/ibanez-premium-rg-870-qmz/gl-ibanez-rg870-1-sml.jpg", description: "6 string guitar loaded with an edge zero 2 with zps2 and cap-vm pickups" },
  { id: 2, name: "Jackson js32 8q", price: 380, photo: "https://fullers-original.s3.amazonaws.com/s3fs-public/product/jackson-js32-8q-dinky%E2%84%A2-transparent-black-40366.jpg", description: "8 string guitar with great touch and feel and good pickups for the price range" },
  { id: 3, name: "Cort x-11", price: 330, photo: "https://images01.olx-st.com/ui/56/98/26/83/o_1544806006_84ec45f40ab7b8c652bde7322bf16a4c.jpg", description: "6 string loaded with emg-hz passive pickups" },
  { id: 4, name: "Harley benton cts-24 blue", price: 220, photo: "https://i.ebayimg.com/00/s/MTAyNFg3Njg=/z/bHoAAOSwwAFbDmAH/$_86.JPG", description: "6 string set neck beautiful guitar loaded with roswell PAF-like pickups, really confy as well" },
  { id: 5,name: "Custom strat", price: 200, photo: "https://images.reverb.com/image/upload/s--y5kVLk9I--/a_exif,c_limit,e_unsharp_mask:80,f_auto,fl_progressive,g_south,h_620,q_90,w_620/v1521217658/sywd8cm6fkaqvh9edimt.jpg", description: "Custom strat gold guitar build from a harley benton body and a amazon maple neck loaded with a single dimarzio crunch lab, wich actually cost more than the rest of the guitar LOL" },
  { id: 6, name: "Ibanez RG 870", price: 670.35, photo: "http://www.guitare-live.com/cours/ibanez-premium-rg-870-qmz/gl-ibanez-rg870-1-sml.jpg", description: "6 string guitar loaded with an edge zero 2 with zps2 and cap-vm pickups" },
  { id: 7, name: "Jackson js32 8q", price: 380, photo: "https://fullers-original.s3.amazonaws.com/s3fs-public/product/jackson-js32-8q-dinky%E2%84%A2-transparent-black-40366.jpg", description: "8 string guitar with great touch and feel and good pickups for the price range" },
  { id: 8, name: "Cort x-11", price: 330, photo: "https://images01.olx-st.com/ui/56/98/26/83/o_1544806006_84ec45f40ab7b8c652bde7322bf16a4c.jpg", description: "6 string loaded with emg-hz passive pickups" },
  { id: 9, name: "Harley benton cts-24 blue", price: 220, photo: "https://i.ebayimg.com/00/s/MTAyNFg3Njg=/z/bHoAAOSwwAFbDmAH/$_86.JPG", description: "6 string set neck beautiful guitar loaded with roswell PAF-like pickups, really confy as well" },
  { id: 10, name: "Custom strat", price: 200, photo: "https://images.reverb.com/image/upload/s--y5kVLk9I--/a_exif,c_limit,e_unsharp_mask:80,f_auto,fl_progressive,g_south,h_620,q_90,w_620/v1521217658/sywd8cm6fkaqvh9edimt.jpg", description: "Custom strat gold guitar build from a harley benton body and a amazon maple neck loaded with a single dimarzio crunch lab, wich actually cost more than the rest of the guitar LOL" }



];
