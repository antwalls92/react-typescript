import { ActionReducerMap, MetaReducer } from '@ngrx/store';
import { GlobalState } from './global.state';

import { productReducer } from './product/product';
import { shoppingCartReducer } from './shopping-cart/shopping-cart';
import { environment } from 'src/environments/environment';

export const GlobalReducer: ActionReducerMap<GlobalState> = {
  products: productReducer,
  shoppingCart: shoppingCartReducer
};

export const metaReducers: MetaReducer<GlobalState>[] = !environment.production ? [] : [];



