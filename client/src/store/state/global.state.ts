import { Product } from 'src/app/public/product/models/product';
import { ShoppingCart } from 'src/app/public/shopping-cart/models/shopping-cart';


export interface GlobalState {
  products: Product[];
  shoppingCart: ShoppingCart;
}

export const initialGlobalState: GlobalState = {
  products: [],
  shoppingCart: new ShoppingCart()
}
