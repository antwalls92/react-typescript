import { initialGlobalState } from '../global.state';
import { Action } from '@ngrx/store';
import { ShoppingCart } from 'src/app/public/shopping-cart/models/shopping-cart';
import { Product } from 'src/app/public/product/models/product';


export function shoppingCartReducer(shoppingCart = initialGlobalState.shoppingCart, action: ShoppingCartAction): ShoppingCart {
  switch (action.type) {

    case ShoppingCartActionTypes.RemoveProduct: {
      shoppingCart.removeProduct(action.payload);
      return shoppingCart;
    }
    case ShoppingCartActionTypes.AddProduct: {
      shoppingCart.addProduct(action.payload);
      return shoppingCart;
    }
  }
}

export enum ShoppingCartActionTypes {

  AddProduct = 'ADD_PRODUCT',
  RemoveProduct = 'REMOVE_PRODUCT',
}


export class AddProduct implements Action {
  readonly type = ShoppingCartActionTypes.AddProduct;
  constructor(public payload: Product) { }
}

export class RemoveProduct implements Action {
  readonly type = ShoppingCartActionTypes.RemoveProduct;
  constructor(public payload: number) { }
}

export type ShoppingCartAction = AddProduct | RemoveProduct;
